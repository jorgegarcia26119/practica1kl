package com.example.practica1corte2

class Operaciones (private var num1:Float,private var num2:Float){
    fun suma(): Float{
        return this.num1 + this.num2
    }
    fun resta(): Float{
        return this.num1 - this.num2
    }
    fun multiplica(): Float{
        return this.num1 * this.num2
    }
    fun dividir(): Float{
        return this.num1 / this.num2
    }
}
